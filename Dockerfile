FROM centos:centos7.9.2009

RUN rpmkeys --import file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 && \
    yum update -y && \
    yum install -y epel-release && \
    yum clean all && \
    rm -r /var/cache/yum && \
    rm -rf /var/lib/apt/lists/*

RUN yum install -y wget make gcc openssl-devel bzip2-devel git && \
    yum clean all && \
    rm -r /var/cache/yum && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://www.python.org/ftp/python/3.8.7/Python-3.8.7.tgz -P /tmp
    
RUN tar xzf /tmp/Python-3.8.7.tgz -C /tmp && \
    rm /tmp/Python-3.8.7.tgz --force

RUN cd /tmp/Python-3.8.7/ && \
    ./configure --enable-optimizations && \
    make altinstall

RUN yum install python3-pip -y && \
    yum clean all && \
    rm -r /var/cache/yum && \
    rm -rf /var/lib/apt/lists/*

RUN /usr/local/bin/python3.8 -m pip install --upgrade pip setuptools && \
    pip3.8 install flask flask-restful

RUN mkdir /tmp/flask-jsonpify && \
    cd /tmp/flask-jsonpify/ && \
    git init && \
    git pull https://github.com/corydolphin/flask-jsonpify.git && \
    python3.8 setup.py install

RUN rm -r /tmp/Python-3.8.7 --force

COPY python-api.py /opt/python_api/python-api.py

EXPOSE 5290

CMD ["python3.8", "/opt/python_api/python-api.py"]
